# README
Vocab trainer to study languages or alike.  
Find it at https://og8.org/turtles/

## Features
Words can be added manually or by importing/parsing the dict.cc vocab lists from https://my.dict.cc/lists/?f=-inlist-DEEN. Permission is granted by Paul from [dict.cc](https://dict.cc), a great collaborative dictionary.  
The code is mostly Ruby on Rails, but with some Javascript added to query the API with AJAX. New cards are loaded in the background.

## Install ruby + rails
On a debian system the repo is easily installed. Other distros are very similar.  

apt-get install curl g++ gcc autoconf automake bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev libtool libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev libreadline-dev libssl-dev

gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB  
curl -sSL https://get.rvm.io | bash -s stable  

source ~/.rvm/scripts/rvm

rvm install 2.6.4  
gem install rails  

ruby -v # 2.6.3  
git clone https://gitlab.com/og8org/turtles/

bundle install

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -  
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list  

curl -sL https://deb.nodesource.com/setup_10.x | bash -

yarn install --check-files

RAILS_ENV=production bundle exec rake db:create db:schema:load
RAILS_ENV=development bundle exec rake db:migrate
RAILS_ENV=production bundle exec rake db:migrate

rake assets:precompile

# adjust config/environments/production.rb to serve the assets right
  config.hosts << "turtles.og8.org"
  config.serve_static_files = true
  config.serve_static_assets = true
  config.assets.compile = true
  config.assets.prefix = "/public/turtles/assets"
  config.assets.digest = true


## Setup server
`rail server` runs it in dev mode.
For production, use `bundle exec puma -e production -b unix:///path/to/turtles.sock` to create a socket connection to nginx for example.
The important part of the nginx config is (adjust the path):
```
upstream app {
    server unix:/path/to/turtles.sock fail_timeout=0;
}
server {
  # your basic config here
  # ...
  location ~ ^/turtles(/.*|$) { try_files $uri @turtles; }
  location @turtles {
      proxy_pass http://app;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
    proxy_redirect off;
  }
}
```

# Screenshots
![](screenshots/covered.png)
![](screenshots/uncovered.png)
