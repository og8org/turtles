class Api::TurtlesController < ApiController
  after_action :set_version_header

  def index
    if params[:index] == "Alle" || params[:index].blank?
      render json: @turtles = Turtle.all.shuffle
    else
      render json: @turtles = Turtle.where(:index=>params[:index]).shuffle
    end
  end

  def show
    render json: @turtle
  end

  def create
    if params[:index].blank?
      @turtle = Turtle.new(turtle_params.merge(index: "Alle"))
    else
      @turtle = Turtle.new(turtle_params)
    end
    @turtle.save
    render json: @turtle
  end

  def difficulty
    @turtle = Turtle.find(params[:id])
    @turtle[:difficulty] = params[:difficulty]
    @turtle.save
  end

  def destroy
    @turtle = Turtle.find(params[:id])
    @turtle.destroy
  end

  def options
  end

  private
    def turtle_params
      params.require(:turtle).permit(:left, :right, :index, :difficulty)
      #params.permit(:left, :right, :index, :difficulty)
    end

  protected
    def set_version_header
        response.headers['Access-Control-Allow-Origin'] = 'https://quickdict.og8.org'
        response.headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = '*, X-Requested-With, X-Prototype-Version, X-CSRF-Token, Content-Type'
    end

end
