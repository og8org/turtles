
class TurtlesController < ApplicationController
  after_action :set_version_header

  def index
    @turtles = Turtle.all
    @index = params[:index]
    @indexe = Turtle.select(:index).where.not(index: "Alle").distinct
  end

  def show
    @turtle = Turtle.find(params[:id])
  end

  def new
    @turtle = Turtle.new()
    @index = params[:index]
  end

  def edit
    @turtle = Turtle.find(params[:id])
  end

  def create
    if params[:turtle][:index].blank?
      @turtle = Turtle.new(turtle_params.merge(index: "Alle"))
    else
      @turtle = Turtle.new(turtle_params)
    end
    if @turtle.save
      redirect_to action: :new, index: turtle_params[:index]
    else
      render 'new'
    end
  end

  def update
    @turtle = Turtle.find(params[:id])
 
    if @turtle.update(turtle_params)
      redirect_to action: 'index'
    else
      render 'edit'
    end
  end

  def destroy
    @turtle = Turtle.find(params[:id])
    @turtle.destroy

    redirect_to action: :list
  end

  def list
    @indexe = Turtle.select(:index).where.not(index: "Alle").distinct
    @turtles = Turtle.all
  end

  def import
  end

  def importme
    url = params[:url]
    inverted = params[:invert]
    if url.start_with?("https://my.dict.cc/print/")
      TurtlesHelper::Parser.new.parse(url, inverted)
    end
    redirect_to action: "list"
  end

  def delete_index
    Turtle.where(index: params[:index]).destroy_all
    redirect_to action: 'list'
  end

  private
    def turtle_params
      params.require(:turtle).permit(:left, :right, :index, :difficulty)
    end

  protected
    def set_version_header
        response.headers['Access-Control-Allow-Origin'] = 'https://quickdict.og8.org https://xt.og8.org'
        response.headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = '*, X-Requested-With, X-Prototype-Version, X-CSRF-Token, Content-Type'
        response.headers['X-Frame-Options'] = 'ALLOW-FROM xt.og8.org'
    end
end
