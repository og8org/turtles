
require 'open-uri'
require 'nokogiri'

module TurtlesHelper

  class Parser

    def parse(url, inverted)

      doc = Nokogiri::HTML(open(url))

      tab = doc.search('table')[1]

      trlist = tab.xpath('tr')

      index = trlist[0].xpath("td")[0].content

      trlist[2..-1].each do |tr|
        td = tr.xpath("td")
        turtle = Turtle.new
        turtle.index = index
        if inverted
          turtle.left = td[1].content
          turtle.right = td[0].content
        else
          turtle.left = td[0].content
          turtle.right = td[1].content
        end
        turtle.difficulty = 1
        turtle.save()

      end

    end
  end

end
