class Turtle < ApplicationRecord
  validates :left, presence: true,
                   length: { minimum: 2 }
end
