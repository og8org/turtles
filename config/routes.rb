Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    resources :turtles
    post 'difficulty/:id/:difficulty', to: 'turtles#difficulty'
    #options 'turtles', action: :options
    match 'turtles', to: 'turtles#options', via: [:options,]
  end

  get 'turtles/list', to: 'turtles#list'
  get 'turtles/import', to: 'turtles#import'
  get 'turtles/importme', to: 'turtles#importme'
  get 'turtles/delete_index', to: 'turtles#delete_index'
  resources :turtles
end

