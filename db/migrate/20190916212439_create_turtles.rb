class CreateTurtles < ActiveRecord::Migration[6.0]
  def change
    create_table :turtles do |t|
      t.string :left
      t.string :right

      t.timestamps
    end
  end
end
