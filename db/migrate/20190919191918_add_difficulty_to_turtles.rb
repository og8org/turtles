class AddDifficultyToTurtles < ActiveRecord::Migration[6.0]
  def change
    add_column :turtles, :difficulty, :integer, default: 0
  end
end
