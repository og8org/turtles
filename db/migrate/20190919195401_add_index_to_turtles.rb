class AddIndexToTurtles < ActiveRecord::Migration[6.0]
  def change
    add_column :turtles, :index, :string
  end
end
