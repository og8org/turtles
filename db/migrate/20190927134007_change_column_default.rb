class ChangeColumnDefault < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:turtles, :difficulty, 1)
  end
end
