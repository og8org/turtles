document.domain = "og8.org"

var bt1;
var bt2;
var hbt1;
var hbt2;
var btoggle = false;
var itoggle = false;
var diff_list = [];
var difflevel;
var id; // id in rails
var membt1 = [];
var membt2 = [];
var memid = [];
var memdiff = [];
var arraycount;
var index;
var turtles = [];
var hostname = "https://turtles.og8.org"
var hdiff;
var htemp;

function loadme() {
  hbt1 = document.getElementById('box1');
  hbt2 = document.getElementById('box2');
  diff_list.push(document.getElementById('leicht'));
  diff_list.push(document.getElementById('ok'));
  diff_list.push(document.getElementById('schwer'));
  hdiff = document.getElementById("difficulty");
  hindex = document.getElementById('divindex');
  difflevel = hdiff ? parseInt(hdiff.value) : 1;
  select(difflevel);
  htemp = document.getElementById("temp_information");
  index = htemp.dataset.hasOwnProperty("index") ? htemp.dataset['index'] : "Alle";
  hilabel = document.getElementById("editindex");
  if (hilabel) hilabel.value = index;
}

function loadnext() {
  arraycount = -1;
  id = 0;
  bt1 = "...";
  bt2 = "";
  hbt1.innerHTML = bt1;
  hbt2.classList.add('whitening');
  difflevel = 1;
  select(difflevel);

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        let data = JSON.parse(xhttp.responseText);
        for(let i=0;i<data.length;i++) {
          membt1.push(data[i].left);
          membt2.push(data[i].right);
          memid.push(data[i].id);
          memdiff.push(data[i].difficulty);
        }
        if (bt1 == "...") next(); // first run
      }
  };
  xhttp.open("GET", hostname+"/api/turtles/?index="+index);
  xhttp.send();
}

function next() {
  close();
  select(1);
  arraycount++;
  bt1 = membt1[arraycount];
  bt2 = membt2[arraycount];
  id = memid[arraycount];
  hbt1.innerHTML = bt1;
  select(memdiff[arraycount]);
  if (membt1.length - arraycount < 3) loadnext();
}

function prev() {
  close();
  select(1);
  if (!arraycount) return
  arraycount--;
  bt1 = membt1[arraycount];
  bt2 = membt2[arraycount];
  id = memid[arraycount];
  hbt1.innerHTML = bt1;
}

function select(idiff) {
  difflevel = idiff;
  for(let i=0;i<3;i++) {
    if (i == idiff) diff_list[i].classList.add("selected");
    else diff_list[i].classList.remove("selected");
  }
}

function clickdiff(idiff, editmode) {
  select(idiff);
  if (editmode) {
    document.getElementById("difficulty").value = idiff;
  }
  else
    sendhome(idiff);
}

function delme() {
  var xhr = new XMLHttpRequest();
  xhr.open('DELETE', hostname + '/api/turtles/'+id, true);
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  //xhr.onload = function () {
      //console.log('deleting',id.toString());
  //};
  xhr.send();
  next();
}

function editme() {
  window.location.href = hostname+'/turtles/'+id+'/edit';
}

function sendhome(idiff) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', hostname + '/api/difficulty/'+id+'/'+idiff, true);
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  //xhr.onload = function () {
      //console.log('changing diff ',id.toString(), ' to ', idiff);
  //};
  xhr.send('id='+id.toString()+'&diff='+idiff.toString());
}

function toggle() {
  (btoggle ? close : show)();
}
function toggle_index() {
  if (itoggle) {
    itoggle = false;
    hindex.style.display = "none";
  }
  else {
    itoggle = true;
    hindex.style.display = "block";
  }
}
function show() {
  hbt2.classList.remove('whitening');
  hbt2.innerHTML = bt2;
  btoggle = true;
}
function close() {
  hbt2.classList.add('whitening');
  hbt2.innerHTML = "";
  btoggle = false;
}

function addEvent(element, eventName, fn) {
    if (element.addEventListener)
        element.addEventListener(eventName, fn, false);
    else if (element.attachEvent)
        element.attachEvent('on' + eventName, fn);
}

addEvent(window, 'load', function(){ loadme(); });
